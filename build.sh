#!/bin/sh

for x in *.md; do
	exec >"${x%.md}.html"
	cat <<EOF
<html><head>
<style>
#text-wrapper {
	text-align: left;
	margin-left: auto;
	margin-right: auto;
	max-width: 1000px;
}
#top-title {
	text-align: center;
	font-size: 36px;
	font-weight: bold;
}
#subtitle {
	text-align: center;
}
</style>
EOF
	head -n 1 "$x" | (IFS='|' read -r TITLE TRAILER;
		printf '<title>%s</title>' "$TITLE"
	)
	cat "${x%.md}-headerincl.txt" 2>/dev/null
	echo '</head><body><div id="top-title"><a href="." style="text-decoration: none; color: #000;">'
	echo 'Aliases 3000</a></div><div id="subtitle">(by Peter H. Jin)</div><hr /><div id="text-wrapper">'
	sed -n '2,$p' "$x" | markdown
	echo '</div></body></html>'
done
