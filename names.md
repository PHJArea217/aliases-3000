Names used by Aliases 3000
# Names used

**Note**: This page reflects the names currently in use as of the "last updated"
date shown below. Any new names used or abandoned after then are shown on a
separate [web app](https://appsvr.peterjin.org/a3krul/), hereinafter the "Rapid
Update List".

**Note**: "Rel freq" means "relative frequency", and higher values indicate that
I use this name more often than others.

**Note**: This list does not include names abandoned prior to 2019-06-12, since
Aliases 3000 and similar concepts were still in development at that time.

## Given names (first names)

**Note** that regardless of the names chosen, personal pronouns for myself are
always he/him/his.

* [**Charles**](https://charles.neon.peterjin.org/) - **in use** since
2019-01-09, rel freq 1
 - Chosen because of Charles Darwin. According to what I've heard, when he
wrote his theory of evolution in "On the Origin of Species", he was scared that
people would make fun of it because it was against the religious theory of
creationism. I don't know for sure that was true, but it nevertheless draws
parallels with the fact that I might have unusual patterns of thinking.
* [**Elizabeth** \(nn **Lizzie**)](https://lizzie.neon.peterjin.org) - **in
use** since 2019-03-20, rel freq 1
 - Initially chosen for a VPS username because I had a friend named "Liz", but
I didn't want to feel like I stole her name, so I chose a similar variant.
Originally wanted to only use it internally, but then one day I decided to
install a mail server on said VPS and used Unix users for convenience, with
the \(somewhat unfortunate) consequence that my \(initially test-only) email
address ended up with "lizzie" in it. Being the weirdo that I can sometimes be,
and because I have actually made some use of that email address, I kept it.
No harm meant to be done to that "Liz", but at least it's a different name.
 - I am not transgender. I chose this name because I liked the versatility of
"Elizabeth" nicknames.
* *Robert* - proposed since 2019-06-10 \(although I might have conceived it
earlier) - chosen because I felt like it was a relatively common name.
* *Anthony* - proposed since 2019-06-13
 - Chosen because of the initial scene \(in 2016) in which I conceived the idea
of using a different name, although it was not brought up until now. I was at
an electrocardiograph screening session at my high school, and the guy doing
the ECG wanted to talk to me. I ended up rambling about something related to
the name "Isaac" \(e.g. Isaac Newton), but when the guy talked to me again, he
mistakenly called me "Isaac". At first I thought he actually wanted to call me
Isaac, and that's where the general idea of being called by a different name
came to be. Later on, I realized that the guy called me that because the word
"Isaac" was in his brain as a result of me rambling about Isaac Newton, but the
idea of calling me "Isaac" \(or a different name in general) still stuck. The
guy giving me the ECG was named Tony.
 - "Isaac" was not the actual name mentioned in the scene above due to privacy
reasons.
* *Joshua* \(nn *Josh*) - proposed since 2019-10-08
 - Chosen \(partly) due to a certain incident I had in one of my senior high
school classes where I mistakenly called someone Josh \(his name was not Josh)
because he "looked like" a Josh. I don't remember which "Josh" I was referring
to \(this was more than a year ago) or which "Josh" he reminded me of, but I
clearly remember how embarrassed I was by that incident since at that time, I
had a lot of trouble remembering faces to names. To protect the innocent, this
name was ultimately chosen not because of this incident, but rather because
it sounded good on its own.
* *Edwin* - proposed since 2019-11-19
 - Not chosen due to anything or anyone \(I'm looking at you Mr. Hubble), but
I just thought this was a cool name, enough to override the requirement that
the names be chosen due to an event or personality characteristic linked to it.
If it is revealed that this coolness was primarily due to Edwin Hubble,
specifically in relation to the Hubble Space Telescope, and not due to the name
itself, this name may be abandoned, but to me, it just sounds like a mash-up
combination of "Edward" \(a common name) and "Goodwin" \(cf. the street east of
the UIUC main campus).


## Surnames (last names)

* **Neonymick** - **in use** since 2019-05-01
 - Represents the "neon" part of the ```neon.peterjin.org``` domain name used by Aliases 3000.
 - Chosen because of "neos-" \(new) + "-nym" \(name), and aptly refers to the
fact that I have chosen new names for myself. To the best of my knowledge, there
is no one out there other than me who is using "Neonymick" (or any similar
variant) as a surname, either for themselves or for a fictional character.
 - Formerly "Neonymik", but the -ik ending was a bit awkward.

More up-to-date changes can be found on the Aliases 3000
[rapid update list](https://appsvr.peterjin.org/a3krul/).

[Back to main page](.).
