Aliases 3000
# Home

*This project is still under development*

If you came here because you were wondering why I referred to myself in an
email \(or somewhere else) as Charles, Elizabeth, or any name other than
"Peter", then here is the gist: As part of Aliases 3000 (shown here), I have
chosen several names for myself as an alternative to my own first and middle
name to refer to myself since in certain contexts, I didn't really like using
my real first and/or middle name.

**In a nutshell**: Aliases 3000 is a system that allows me to use names other
than my own to refer to myself in various social situations, without intent to
confuse or mislead others.

Aliases 3000 involves making up personal names for myself, and then being able
to use those names in various situations in addition to my real name.

* Manifesto \(coming soon!)
* Name Selection Process \(coming soon!)
* Presentation \(coming soon!)
* [Name List](names.html)
* [Where the names are used and how I use them](usage.html)
* [FAQ](faq.html)
* [How it all started](https://www.peterjin.org/blog/aliases3k.html)
* [Miscellaneous Information](misc.html)

If you have any questions or comments regarding Aliases 3000, you may post them
either on the comments section of the original
[blog post](https://www.peterjin.org/blog/aliases3k.html) or privately at
peter \[at] peterjin.org.

---

Copyright 2019 Peter Jin \(or \[anything] Neonymick).
View this page on
[GitHub](https://github.com/PHJArea217/aliases-3000)
and
[GitLab](https://gitlab.com/PHJArea217/aliases-3000).
